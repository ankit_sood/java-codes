package com.designpattern.singleton;

public class StaticBlockSingleton {
	private static StaticBlockSingleton staticBlockSingleton; 

	// private constructor to avoid client applications to use constructor
	private StaticBlockSingleton(){
		
	}

	//Memory is assigned in static block. It allows us to handle exceptions.
	static {
		try{
			staticBlockSingleton = new StaticBlockSingleton();
		}catch(Exception e){
			System.out.println("Error occured.");
		}
	}
	
	public static StaticBlockSingleton getInstance(){
		return staticBlockSingleton;
	}
}
