package com.designpattern.singleton;

public class ThreadSafeSingleton {
	private static ThreadSafeSingleton instance;
	
	private ThreadSafeSingleton(){}
	
	//This method works but have higher costs, So double checked locking principle is used.
	/*public static synchronized ThreadSafeSingleton getInstance(){
		if(instance == null){
			instance = new ThreadSafeSingleton();
		}
		return instance;
	}*/
	
	public static ThreadSafeSingleton getInstance(){
		if(instance == null){
			synchronized(ThreadSafeSingleton.class){
				if(instance==null){
					instance = new ThreadSafeSingleton();
				}
			}
		}
		return instance;
	}
}
