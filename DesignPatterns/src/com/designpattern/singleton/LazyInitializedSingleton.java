package com.designpattern.singleton;

public class LazyInitializedSingleton {
	private static LazyInitializedSingleton instance;
	
	private LazyInitializedSingleton(){}
	
	//This works for single thread only hence not thread safe.
	public static LazyInitializedSingleton getInstance(){
		if(instance == null){
			instance = new LazyInitializedSingleton();
		}
		return instance;
	}
}
