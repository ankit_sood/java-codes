package com.thread.examples;
/** 
 * This tutorial shows how to terminate a thread gracefully.Also, it shows where and how we should use volatile keyword.
 * 
 * **/


import java.util.Scanner;

class Processor extends Thread{
	//volatile keyword prevents threads from caching variables when they are not changed with in the thread.
	private volatile boolean isRunning = true;
	
	@Override
	public void run() {
		while(isRunning){
			System.out.println("Hello !! It's processor.");
			try{
				Thread.sleep(3000);
			}catch(InterruptedException exp){
				System.out.println(exp.getMessage());
			}
		}
	}
	
	public void shutdown(){
		isRunning = false;
	}
	
}

public class VolatileKeywordExample {
	public static void main(String args[]){
		Processor proc = new Processor();
		proc.start();
		
		System.out.println("Press enter to stop");
		Scanner scanner = new Scanner(System.in);
		scanner.nextLine();
		
		proc.shutdown();
	}
}
