package com.thread.examples;


public class SynchronizedKeywordExample {
	private int count =0;
	
	public synchronized void increment(){
		count++;
	}
	
	public void doWork() {
		Thread t1 = new Thread(new Runnable() {
			public void run() {
				for(int i=0;i<10000;i++){
					increment();
					//count++;
				}
			}
		});
		
		Thread t2 = new Thread(new Runnable() {
			public void run() {
				for(int i=0;i<10000;i++){
					increment();
					//count++;
				}
			}
		});
		
		t1.start();
		t2.start();
		
		try {
			//join method let's the calling method wait until the execution of the joining thread is completed
			t1.join();
			t2.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println("Count is :"+count);
	}

	public static void main(String[] args) {
		SynchronizedKeywordExample example = new SynchronizedKeywordExample();
		example.doWork();

	}

}
