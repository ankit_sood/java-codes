package com.thread.examples;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Worker {
	private Random random = new Random();
	private List<Integer> list1 = new ArrayList<>();
	private List<Integer> list2 = new ArrayList<>();

	private Object lockForList1 = new Object();
	private Object lockForList2 = new Object();
	
	// This marks the first stage of work done by worker
	public void  partOneWork() throws InterruptedException {
		synchronized(lockForList1){
			Thread.sleep(20);
			list1.add(random.nextInt(100));
		}
	}

	// This marks the second stage of work done by worker
	public synchronized void partTwoWork() throws InterruptedException {
		synchronized(lockForList2){
			Thread.sleep(20);
			list2.add(random.nextInt(100));
		}
	}

	public void process() throws InterruptedException {
		for (int i = 0; i < 100; i++) {
			partOneWork();
			partTwoWork();
		}
	}

	public void main() throws InterruptedException {
		System.out.println("Worker starts doing work-->");

		long start = System.currentTimeMillis();
		Thread t1 = new Thread(new Runnable() {
			public void run() {
				try {
					process();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		Thread t2 = new Thread(new Runnable() {
			public void run() {
				try {
					process();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		t1.start();
		t2.start();
		
		t1.join();
		t2.join();
		long end = System.currentTimeMillis();

		System.out.println("Time taken :" + (end - start) / 1000);
		System.out.println("List 1 size: " + list1.size() + " List 2 size: " + list2.size());
	}
}
