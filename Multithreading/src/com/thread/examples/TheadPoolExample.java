package com.thread.examples;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

class WorkAllocater implements Runnable{
	private int id;
	public WorkAllocater(int id){
		this.id = id;
	}
	
	public void run() {
	System.out.println("Starting : "+id);	
	try {
		Thread.sleep(2000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	System.out.println("Completing : "+id);	
	}
	
}
public class TheadPoolExample {
	public static void main(String args[]){
		ExecutorService executor = Executors.newFixedThreadPool(2);
		
		for(int i=0;i<10;i++){
			executor.submit(new WorkAllocater(i));
		}
		executor.shutdown();
		System.out.println("All tasks submitted successfully");
		try {
			executor.awaitTermination(1, TimeUnit.HOURS);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
