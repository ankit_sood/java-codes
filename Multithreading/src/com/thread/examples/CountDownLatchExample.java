package com.thread.examples;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class ProcessCountDownLatch implements Runnable{
	private CountDownLatch latch = null;

	public ProcessCountDownLatch(CountDownLatch latch){
		this.latch = latch;
	}
	public void run() {
		System.out.println("Thread started!");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		latch.countDown();
	}
	
}

public class CountDownLatchExample {
	public static void main(String args[]){
		CountDownLatch countDownLatch = new CountDownLatch(3);
		ExecutorService executor = Executors.newFixedThreadPool(3);
		for(int i=0;i<3;i++){
			executor.submit(new ProcessCountDownLatch(countDownLatch));
		}
		try {
			countDownLatch.await();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Completed");
	}
}
