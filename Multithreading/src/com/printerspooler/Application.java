package com.printerspooler;

public class Application {
	public static void main(String args[]) throws InterruptedException{
		final PrinterSpooler printerSpooler = new PrinterSpooler();
		Thread producerOne = new Thread(new Runnable(){
			@Override
			public void run() {
				try {
					printerSpooler.printerProducerOne();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
		
		Thread producerTwo = new Thread(new Runnable(){
			@Override
			public void run() {
				try {
					printerSpooler.printerProducerTwo();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

			}
		});		
		Thread printer = new Thread(new Runnable(){
			@Override
			public void run() {
				try {
					printerSpooler.print();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

			}
		});
		
		Thread scheduler = new Thread(new Runnable(){
			@Override
			public void run() {
				try {
					printerSpooler.scheduler();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

			}
		});
		
		producerOne.start();
		producerTwo.start();
		printer.start();
		scheduler.start();
		
		producerOne.join();
		producerTwo.join();
		printer.join();
		scheduler.join();
	}

}
