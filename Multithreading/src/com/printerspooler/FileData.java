package com.printerspooler;

public class FileData {
	private String data;
	private String name;
	private Integer size;
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getSize() {
		return size;
	}
	public void setSize(Integer size) {
		this.size = size;
	}
	
	@Override
	public String toString() {
		return "FileData [data=" + data + ", name=" + name + ", size=" + size + "]";
	}
	

}
