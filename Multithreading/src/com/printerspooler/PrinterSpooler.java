package com.printerspooler;

import java.util.LinkedList;

public class PrinterSpooler {
	private LinkedList<PrinterData> inputList = new LinkedList<>();
	private LinkedList<PrinterData> printerList = new LinkedList<>();
	//private Object inputLock = new Object();
	private Object inputListLock = new Object();
	private Object printerLock = new Object();
	private Boolean printing = false;
	//private int LIMIT = 5;
	
	public void print() throws InterruptedException{
		/*Thread.sleep(1000);
		FileData fileData = null;
		while(true){
			synchronized(printerListLock){
				while(printerList.size()==0){
					printerListLock.wait();
				}
				fileData = printerList.poll().getFileData();
				printerListLock.notify();
				System.out.println(fileData);
			}
		}*/
		PrinterData printerData  = null;
		while(true){
			System.out.println("Running Printer Thread----------->");
			synchronized (printerLock) {
				while(!printing){
					printerLock.wait();
				}
				printerData = printerList.poll();
				printing = false;
				printerLock.notify();
			}
			//printing
			System.out.println(printerData.getFileData());
		}
	}
	
	
	public void printerProducerOne() throws InterruptedException{
		int i = 1;
		while(true){
			System.out.println("Running Producer1 Thread----------->");
			synchronized (inputListLock) {
				PrinterData printerData = new PrinterData();
				printerData.setFileData(getFileData(i));
				inputList.add(printerData);
				i++;
				inputListLock.notify();
			}			
			Thread.sleep(2000);
		}
	}
	
	public void printerProducerTwo() throws InterruptedException{
		int i = 101;
		//Thread.sleep(2000);
		while(true){
			System.out.println("Running Producer2 Thread----------->");
			synchronized (inputListLock) {
				PrinterData printerData = new PrinterData();
				printerData.setFileData(getFileData(i));
				inputList.add(printerData);
				i++;
				inputListLock.notify();
			}
			//Thread.sleep(1000);
		}
	}
	
	public void scheduler() throws InterruptedException{
		while(true){
			System.out.println("Running Scheduler Thread----------->");
			PrinterData printerData = null;
			synchronized(inputListLock){
				while(inputList.size()==0){
					inputListLock.wait();
				}
				System.out.println("PrinterList size is -----------> "+inputList.size());
				printerData = inputList.poll();
				inputListLock.notify();
				synchronized (printerLock) {
					while(printing){
						printerLock.wait();
					}
					System.out.println("Accquired Lock for PrinterList----------->");
					printerList.add(printerData);
					this.printing = true;
					printerLock.notify();
				}
				System.out.println("PrinterList size is -----------> "+inputList.size());
			}
		}
		
	}
	
	private FileData getFileData(int i){
		FileData fileData = new FileData();
		if(i%2==0){
			fileData.setData("*** Even data: i="+i+ "***");
			fileData.setName("file:"+i);
			fileData.setSize(2);
		}else{
			fileData.setData("*** Odd data: i="+i+ "***");
			fileData.setName("file:"+i);
			fileData.setSize(3);
		}
		return fileData;
	}

}
