package com.waitnotify.example;
import java.util.Scanner;

public class WaitNotifyProcessor {
	public void produce() throws InterruptedException{
		synchronized(this){
			System.out.println("Producer thread starts running");
			wait();
			System.out.println("Producer Resumed");
		}
	}
	
	public void consume() throws InterruptedException{
		Scanner scanner = new Scanner(System.in);
		Thread.sleep(4000);
		synchronized(this){
			System.out.println("Waiting for user to press ENTER");
			scanner.nextLine();
			System.out.println("Enter was pressed");
			notify();
			Thread.sleep(5000);
		}
	}
}
