package com.waitnotify.example;

public class WaitNotifyLowLevel {
	public static void main(String args[]) throws InterruptedException{
		WaitNotifyLowLevelProcessor waitNotifyLowLevelProcessor = new WaitNotifyLowLevelProcessor();
		Thread t1 = new Thread(new Runnable(){
			public void run() {
				try {
					waitNotifyLowLevelProcessor.producer();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
		});
		
		Thread t2 = new Thread(new Runnable(){
			public void run() {
				try {
					waitNotifyLowLevelProcessor.consumer();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
		});
		t1.start();
		t2.start();
		
		t1.join();
		t2.join();
	}
}
