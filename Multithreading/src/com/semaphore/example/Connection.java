package com.semaphore.example;

import java.util.concurrent.Semaphore;

public class Connection {
	private int connection = 0;

	private Semaphore semaphore = new Semaphore(10,true);

	public static Connection instance = new Connection();

	// By doing this we can make our class singleton
	private Connection() {

	}

	public static Connection getInstance() {
		return instance;
	}

	public void connect() throws InterruptedException {
		try {
			semaphore.acquire();
			synchronized (this) {
				connection++;
				System.out.println("Current connections: " + connection);
			}

			Thread.sleep(2000);

			synchronized (this) {
				connection--;
				System.out.println("Current connections: " + connection);
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			semaphore.release();
		}
	}
}
