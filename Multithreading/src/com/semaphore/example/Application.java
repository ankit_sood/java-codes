package com.semaphore.example;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class Application {
	public static void main(String[] args) throws InterruptedException {
		//Semaphore sem = new Semaphore(1);
		//sem.acquire();	//method that decrements the availble permits
		//sem.release();	//method that increments the availble permits
		//System.out.println("Available permits:"+sem.availablePermits());
		Connection.getInstance().connect();
		ExecutorService executor = Executors.newCachedThreadPool();
		for(int i=0;i<200;i++){
			executor.submit(new Runnable(){
				public void run() {
					try {
						Connection.getInstance().connect();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
		}
		executor.shutdown();
		
		executor.awaitTermination(1, TimeUnit.DAYS);
	}

}
