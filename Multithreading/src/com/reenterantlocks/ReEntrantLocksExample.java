package com.reenterantlocks;
import java.util.Scanner;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ReEntrantLocksExample {
	private int count = 0;
	private Lock lock = new ReentrantLock();
	private Condition cond = lock.newCondition();
	
	private void increment(){
		for(int i=0;i<10000;i++)
			count++;
	}
	
	public void runFirstThread() throws InterruptedException{
		System.out.println("First thread started");
		lock.lock();
		cond.await();
		System.out.println("First thread is now awake");
		try{
			increment();
		}finally{
			lock.unlock();
		}
	}
	
	public void runSecondThread()throws InterruptedException{
		System.out.println("Second thread started");
		lock.lock();
		Thread.sleep(2000);
		System.out.println("Second thread is now awake");
		System.out.println("Press Enter key to start the process");
		new Scanner(System.in).nextLine();
		System.out.println("Processing of second thread starts");
		try{
			increment();
		}finally{
			cond.signal();
			lock.unlock();
		}
	}
	
	public void finished(){
		System.out.println("The value of count is :"+count);
	}
}
