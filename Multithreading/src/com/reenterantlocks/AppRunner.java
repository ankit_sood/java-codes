package com.reenterantlocks;

public class AppRunner {
	public static void main(String args[]) throws InterruptedException{
		ReEntrantLocksExample reEntrantLocksExample = new ReEntrantLocksExample();
		Thread t1 = new Thread(new Runnable(){
			public void run() {
				try {
					reEntrantLocksExample.runFirstThread();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
		Thread t2 = new Thread(new Runnable(){
			public void run() {
				try {
					reEntrantLocksExample.runSecondThread();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
		t1.start();
		t2.start();
		
		t1.join();
		t2.join();
		
		reEntrantLocksExample.finished();
		
	}
}
