package com.deadlocks.example;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Runner {
	private Account acc1 = new Account();
	private Account acc2 = new Account();
	
	private Lock lock1 = new ReentrantLock();
	private Lock lock2 = new ReentrantLock();
	
	private void acquireLocks(Lock firstLock,Lock secondLock) throws InterruptedException{
		while(true){
			//Acquire Locks 
			boolean gotFirstLock = false;
			boolean gotSecondLock = false;
			try{
				gotFirstLock = firstLock.tryLock(3, TimeUnit.SECONDS);
				gotSecondLock = secondLock.tryLock(3, TimeUnit.SECONDS);;
			}finally{
				//checking if both the locks are acquired
				if(gotFirstLock && gotSecondLock){
					return;
				}
				//if only one is acquired, we need to release that since it may be starving other threads
				if(gotFirstLock){
					firstLock.unlock();
				}
				if(gotSecondLock){
					secondLock.unlock();
				}
			}
			//or Sleep long enough so that other threads has released the lock
			Thread.sleep(1000);
		}
	}
	/** 
	 * In this example both the threads can have locks in order lock1 and lock2 or lock2 and lock1. So, if thread 1 have locks like
	 * lock1, lock2 and thread 2 have locks like lock2,lock1. It will create the deadlock. So to solve this we have designed a method named acquirelocks.
	 * For production scenario, we should implement timeouts with each thread as we don't want any thread to wait for long in the production 
	 * environment.
	 * @throws InterruptedException 
	 * **/
	
	public void firstThread() throws InterruptedException {
		Random random = new Random();
		for(int i=0;i<100;i++){
			acquireLocks(lock1,lock2);
			try{
				Account.tarnsfer(acc1, acc2, random.nextInt(100));
			}finally{
				lock1.unlock();
				lock2.unlock();
			}
		}
	}

	public void secondThread() throws InterruptedException {
		Random random = new Random();
		for(int i=0;i<100;i++){
			acquireLocks(lock1,lock2);
			try{
				Account.tarnsfer(acc2, acc1, random.nextInt(100));
			}finally{
				lock1.unlock();
				lock2.unlock();
			}
		}
	}

	public void finished() {
		System.out.println("Account 1 balance: "+acc1.getBalance());
		System.out.println("Account 2 balance: "+acc2.getBalance());
		System.out.println("Total Balance: "+(acc1.getBalance()+acc2.getBalance()));
	}
}
