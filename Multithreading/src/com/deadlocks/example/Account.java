package com.deadlocks.example;

public class Account {
	private int balance = 10000;
	
	public void deposit(int amount){
		balance = balance + amount;
	}
	
	public void withdraw(int amount){
		balance = balance - amount;
	}
	
	public int getBalance(){
		return balance;
	}
	
	public static void tarnsfer(Account acc1,Account acc2,int amount){
		acc1.withdraw(amount);
		acc2.deposit(amount);
	}

}
