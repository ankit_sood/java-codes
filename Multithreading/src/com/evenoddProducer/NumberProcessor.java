package com.evenoddProducer;

import java.util.LinkedList;

public class NumberProcessor {
	private LinkedList<Integer> list = new LinkedList<Integer>();
	private final int limit = 10;
	private Object lock = new Object();
	private int value = 0;

	public void produceOdd() throws InterruptedException {
		while (list.size()<limit) {
			synchronized (lock) {
				while(list.size()%2==0){
					lock.wait();
				}
				list.add(value++);
				System.out.println("After Odd number addition list is :");
				lock.notify();
			}
		}
	}
	
	public void produceEven() throws InterruptedException {
		while (list.size()<limit) {
			synchronized (lock) {
				while(list.size()%2!=0){
					lock.wait();
				}
				list.add(value++);
				System.out.println("After Even number addition list is :" + list + list.size());
				lock.notify();
			}
		}
	}
}
