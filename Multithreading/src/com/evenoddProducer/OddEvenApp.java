package com.evenoddProducer;

public class OddEvenApp {
	public static void main(String args[]){
		final NumberProcessor processor = new NumberProcessor();
		Thread t1 = new Thread(new Runnable(){
			public void run() {
				try {
					processor.produceOdd();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		Thread t2 = new Thread(new Runnable(){
			public void run() {
				try {
					processor.produceEven();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		t1.start();
		t2.start();
		try {
			t1.join();
			t2.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	
	}
}
