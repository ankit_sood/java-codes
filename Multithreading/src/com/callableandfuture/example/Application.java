package com.callableandfuture.example;

import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Application {
	public static void main(String args[]) throws InterruptedException, ExecutionException{
		ExecutorService executor = Executors.newFixedThreadPool(2);
		
		Future<Integer> future = executor.submit(new Callable<Integer>(){
			@Override
			public Integer call() throws Exception {
				System.out.println("Thread execution starts");
				Random random = new Random();
				Thread.sleep(3000);
				System.out.println("Thread execution finishes");
				return random.nextInt(4000);
			}
			
		});
		
		executor.shutdown();
		System.out.println("Duration is "+future.get());
	}
}
