package graph.bfs;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

public class BreadthFirstSearch {
	Queue<Integer> queue = new LinkedList<Integer>();
	
	private int[][] formAdjecencyMatrix(){
		int[][] adjecencyMatrix = {{0,1,1,1,0},{1,0,0,0,1},{1,0,0,0,1},{1,0,0,0,1},{0,1,1,1,0}};
		return adjecencyMatrix;
	}
	
	private void performBFS(int sourceVertex,Map<Integer,List<Integer>> adjecencyMap){
		List<Integer> bfsList = new ArrayList<Integer>();
		queue.add(sourceVertex);
		while(!queue.isEmpty()){
			int currentVertex = queue.poll();
			List<Integer> verticesList = adjecencyMap.get(currentVertex);
			for(int vertices:verticesList){
				if(!bfsList.contains(vertices) && !queue.contains(vertices)){
					queue.add(vertices);
				}
			}
			bfsList.add(currentVertex);
		}
		System.out.println(bfsList.toString());
	}
	
	public static void main(String args[]){
		BreadthFirstSearch bfs = new BreadthFirstSearch();
		Map<Integer,List<Integer>>  adjecencyMap = new HashMap<Integer,List<Integer>>();
		int[][] adjecencyMatrix = bfs.formAdjecencyMatrix();
		List<Integer> vertexList = null;
		for(int i=0;i<5;i++){
			for(int j=0;j<5;j++){
				if(adjecencyMatrix[i][j]==1){
					if(adjecencyMap.get(i)!=null){
						vertexList = adjecencyMap.get(i);
					}else{
						vertexList = new ArrayList<Integer>();
					}
					vertexList.add(j);
					adjecencyMap.put(i,vertexList);
				}
			}
		}
		int sourceVertex = 0;
		bfs.performBFS(sourceVertex,adjecencyMap);
	}
}
