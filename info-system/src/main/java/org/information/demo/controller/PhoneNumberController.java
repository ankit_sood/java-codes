package org.information.demo.controller;

import org.information.demo.response.PhoneNumberResponse;
import org.information.demo.response.PhoneNumberResponseBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;

@RestController
@RequestMapping("/contactnumber")
public class PhoneNumberController {

	@GetMapping("/validate/{phoneNumber}/{country}")
	public ResponseEntity<PhoneNumberResponse> validatePhoneNumber(@PathVariable("phoneNumber")String phoneNumber,
			@PathVariable("country")String country) throws NumberParseException{
		PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
		PhoneNumber parsedNumber = phoneUtil.parse(phoneNumber,country);
		boolean isValid = phoneUtil.isValidNumber(parsedNumber);
		PhoneNumberResponseBuilder phoneNumberResponseBuilder = new PhoneNumberResponseBuilder(isValid,parsedNumber.getCountryCode(),parsedNumber.getNationalNumber());
		return ResponseEntity.ok(phoneNumberResponseBuilder.build());
	}
}
