package org.information.demo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.google.i18n.phonenumbers.NumberParseException;

@ControllerAdvice
public class ExceptionAdvice {
	
	@ResponseBody
	@ExceptionHandler(NumberParseException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	String phoneNumberParseExceptionHandler(NumberParseException npe) {
		return "Unable to parse the Phone Number.";
	}
	
	@ResponseBody
	@ExceptionHandler(Exception.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	String phoneNumberParseExceptionHandler(Exception exp) {
		return "Something went wrong with the server.";
	}
}	
