package org.information.demo.response;

public final class PhoneNumberResponse {
	//All are required parameters
	private Boolean isValid;
	private String countryCode;
	private String nationalNumber;
	
	public Boolean getIsValid() {
		return isValid;
	}
	public void setIsValid(Boolean isValid) {
		this.isValid = isValid;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getNationalNumber() {
		return nationalNumber;
	}
	public void setNationalNumber(String nationalNumber) {
		this.nationalNumber = nationalNumber;
	}
	
	PhoneNumberResponse(PhoneNumberResponseBuilder phoneNumberResponseBuilder) {
		this.isValid = phoneNumberResponseBuilder.getIsValid();
		this.countryCode = phoneNumberResponseBuilder.getCountryCode();
		this.nationalNumber = phoneNumberResponseBuilder.getNationalNumber();
	}
}
