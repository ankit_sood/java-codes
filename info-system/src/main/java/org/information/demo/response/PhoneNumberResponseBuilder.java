package org.information.demo.response;

public class PhoneNumberResponseBuilder {
	private Boolean isValid;
	private String countryCode;
	private String nationalNumber;
	
	public PhoneNumberResponseBuilder(Boolean isValid,String countryCode,String nationalNumber) {
		this.isValid = isValid;
		this.countryCode = countryCode;
		this.nationalNumber = nationalNumber;
	}
	
	public PhoneNumberResponseBuilder(Boolean isValid,Integer countryCode,Long nationalNumber) {
		this.isValid = isValid;
		this.countryCode = countryCode.toString();
		this.nationalNumber = nationalNumber.toString();
	}
	
	public PhoneNumberResponse build() {
		return new PhoneNumberResponse(this);
	}

	public Boolean getIsValid() {
		return isValid;
	}

	public void setIsValid(Boolean isValid) {
		this.isValid = isValid;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getNationalNumber() {
		return nationalNumber;
	}

	public void setNationalNumber(String nationalNumber) {
		this.nationalNumber = nationalNumber;
	}
}
