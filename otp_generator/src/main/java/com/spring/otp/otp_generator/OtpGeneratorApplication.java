package com.spring.otp.otp_generator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


//@EnableJpaRepositories("com.shri.repo")
//@EntityScan("com.shri.model")
@EnableAutoConfiguration(exclude = {SecurityAutoConfiguration.class})//to bypass this spring boot default security mechanism.
@SpringBootApplication
public class OtpGeneratorApplication {

	public static void main(String[] args) {
		SpringApplication.run(OtpGeneratorApplication.class, args);
	}
}
