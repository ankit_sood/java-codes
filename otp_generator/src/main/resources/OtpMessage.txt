Hi {{user}},

Your secure code for the current transaction is {{otpnum}}. Code will only be valid for 10 minutes.
Please do not share any valuable information with anyone.

Thanks,
Ankit Sood