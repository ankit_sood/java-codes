package com.shimlauniversity.contoller;


import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController{

	@RequestMapping(value = {"/index/{id}/{userName}"})
	public ModelAndView getHomePage(@PathVariable Map<String,String> pathVars) {
		ModelAndView modelAndView = new ModelAndView("HelloPage");
		
		modelAndView.addObject("welcomeMessage", "You are on the home page of the Shimla University.");
		modelAndView.addObject("userName",pathVars.get("userName"));
		modelAndView.addObject("id",pathVars.get("id"));
		return modelAndView;
	}
	
	

}
