package com.decathlon.orderapi.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.decathlon.orderapi.constants.OrderConstants;
import com.decathlon.orderapi.util.CustomDateSerializer;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.Data;

@Data
@Entity
@Table(name="CUST_ORDER")
public class CustomerOrder implements Serializable{
	private static final long serialVersionUID = -7376294291230785271L;

	@Id
	@GeneratedValue
	@Column(name="ORDER_ID")
	private Long orderId;
	
	@NotNull(message=OrderConstants.CUSTOMER_NAME_EMPTY_ERROR)
	@Column(name="CUSTOMER_NAME")
	private String customerName;
	
	@NotNull(message=OrderConstants.MODE_OF_BUYING_EMPTY_ERROR)
	@Column(name="MODE_OF_BUYING")
	private String modeOfBuying;
	
	@JsonSerialize(using=CustomDateSerializer.class)
	@NotNull(message=OrderConstants.ORDER_DATE_EMPTY_ERROR)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
	@Temporal(TemporalType.DATE)
	@Column(name="ORDER_DATE")
	private Date orderDate;
	
	@Column(name="ORDER_STATUS")
	private String orderStatus;
}
