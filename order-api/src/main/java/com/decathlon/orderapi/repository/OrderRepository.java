package com.decathlon.orderapi.repository;

import org.springframework.data.repository.CrudRepository;

import com.decathlon.orderapi.model.CustomerOrder;

public interface OrderRepository extends CrudRepository<CustomerOrder, Long>{

}
