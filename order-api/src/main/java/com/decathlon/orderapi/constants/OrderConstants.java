package com.decathlon.orderapi.constants;

public class OrderConstants {
	public static final String ONLINE_SHIPPED_CANCELLED_ERROR_MSG="Orders can not be cancelled After Shipping.";
	public static final String ONLINE_CANCELLED_SHIPPED_ERROR_MSG="Orders can not be Shipped After Cancellation.";
	public static final String ORDER_NOT_AVAILBLE_WITH_ID_ERROR_MSG="Could not find order with ID ";
	public static final String CUSTOMER_NAME_EMPTY_ERROR ="Customer Name should not be empty.";
	public static final String MODE_OF_BUYING_EMPTY_ERROR ="Mode of Buying should not be empty.";
	public static final String ORDER_DATE_EMPTY_ERROR ="Order Date should not be empty.";
}
