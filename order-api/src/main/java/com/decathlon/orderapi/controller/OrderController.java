package com.decathlon.orderapi.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.decathlon.orderapi.exception.InvalidOrderStatusException;
import com.decathlon.orderapi.model.CustomerOrder;
import com.decathlon.orderapi.service.OrderService;
import com.decathlon.orderapi.valueobjects.OrderStatusEnum;
import com.decathlon.orderapi.valueobjects.OrderVO;

@RestController
@RequestMapping(value="/orders")
public class OrderController {
	
	private final OrderService orderService;
	
	@Autowired
	OrderController(OrderService orderService){
		this.orderService = orderService;
	}
	
	@GetMapping(produces = MediaTypes.HAL_JSON_VALUE)
	public Collection<Resource<CustomerOrder>> getOrders() throws Exception{
		Collection<Resource<CustomerOrder>> root = new ArrayList<>();
		Collection<CustomerOrder> customerOrders = this.orderService.getOrders();
		root.addAll(customerOrders.stream().map(custOrder -> {
			try {
				return toResource(custOrder, custOrder.getOrderId());
			} catch (Exception e) {
				// TODO Auto-generated catch block
			}
			return null;
		}).collect(Collectors.toList()));
		return root;
	}

	@GetMapping("/{id}")
	public ResponseEntity<CustomerOrder> getOrdersById(@PathVariable("id")Long orderId) throws Exception {
		return ResponseEntity.ok(this.orderService.getOrdersById(orderId));
	}
	
	@PostMapping
	public ResponseEntity<?> addOrders(@Valid @RequestBody CustomerOrder customerOrder,BindingResult result) {
		if(result.hasErrors()) {
			List<String> errorList = result.getAllErrors().stream().map(ObjectError::getDefaultMessage).collect(Collectors.toList());
			return ResponseEntity.badRequest().body(errorList);
		}else {
			return this.orderService.addOrders(customerOrder).map(t -> {
				URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(t.getOrderId()).toUri();
				return ResponseEntity.created(location).build();
			}).orElse(ResponseEntity.noContent().build());
		}
	}
	
	@PutMapping
	public ResponseEntity<CustomerOrder> updateOrders(@RequestBody OrderVO orderVO) throws Exception {
		CustomerOrder customerOrder = this.orderService.getOrdersById(orderVO.getOrderId());
		if(!(OrderStatusEnum.CANCELLED.getValue().equalsIgnoreCase(orderVO.getOrderStatus()) || 
				OrderStatusEnum.SHIPPED.getValue().equalsIgnoreCase(orderVO.getOrderStatus()))) {
			throw new InvalidOrderStatusException(customerOrder.getOrderId(),orderVO.getOrderStatus());
		}
		return this.orderService.updateOrders(orderVO,customerOrder).map(t -> {
			return ResponseEntity.ok(t);
		}).orElse(ResponseEntity.noContent().build());
	}
	
	private static Resource<CustomerOrder> toResource(CustomerOrder customerOrder, Long orderId) throws Exception {
		return new Resource<>(customerOrder,ControllerLinkBuilder.linkTo(
												ControllerLinkBuilder.methodOn(OrderController.class)
																	 .getOrdersById(orderId)).withSelfRel());
	}
	
}
