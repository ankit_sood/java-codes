package com.decathlon.orderapi.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.decathlon.orderapi.exception.InvalidOrderStatusException;
import com.decathlon.orderapi.exception.OrderNotFoundException;
import com.decathlon.orderapi.model.CustomerOrder;
import com.decathlon.orderapi.repository.OrderRepository;
import com.decathlon.orderapi.valueobjects.ModeOfBuyingEnum;
import com.decathlon.orderapi.valueobjects.OrderStatusEnum;
import com.decathlon.orderapi.valueobjects.OrderVO;

import static com.decathlon.orderapi.constants.OrderConstants.ONLINE_SHIPPED_CANCELLED_ERROR_MSG;
import static com.decathlon.orderapi.constants.OrderConstants.ONLINE_CANCELLED_SHIPPED_ERROR_MSG;
import static com.decathlon.orderapi.constants.OrderConstants.ORDER_NOT_AVAILBLE_WITH_ID_ERROR_MSG;

@Service
public class OrderService {
	
	private final OrderRepository orderRepository;
	
	@Autowired
	public OrderService(OrderRepository orderRepository) {
		this.orderRepository = orderRepository;
	}
	
	public List<CustomerOrder> getOrders() {
		List<CustomerOrder> ordersList = new ArrayList<>();
		orderRepository.findAll().forEach(order -> ordersList.add(order));
		return ordersList;
	}
	
	public CustomerOrder getOrdersById(Long orderId) throws OrderNotFoundException{
		return this.orderRepository.findById(orderId).
				orElseThrow(()->new OrderNotFoundException(ORDER_NOT_AVAILBLE_WITH_ID_ERROR_MSG +" '" + orderId + "'."));
	}
	
	public Optional<CustomerOrder> addOrders(CustomerOrder customerOrder) {
		return Optional.ofNullable(orderRepository.save(setOrderStatus(customerOrder)));
	}
	
	public Optional<CustomerOrder> updateOrders(OrderVO orderVO,CustomerOrder customerOrder) throws InvalidOrderStatusException{
		validateOrderStatus(orderVO, customerOrder);
		return Optional.ofNullable(orderRepository.save(customerOrder));
	}
		
	private CustomerOrder setOrderStatus(CustomerOrder order) {
		if(ModeOfBuyingEnum.ONLINE.getValue().equals(order.getModeOfBuying())) {
			order.setOrderStatus(OrderStatusEnum.PRPEPARED_FOR_SHIPPING.getValue());
		}else if(ModeOfBuyingEnum.STORE.getValue().equals(order.getModeOfBuying())){
			order.setOrderStatus(OrderStatusEnum.SHIPPED.getValue());
		}
		return order;
	}
	
	private CustomerOrder validateOrderStatus(OrderVO orderVO,CustomerOrder customerOrder) throws InvalidOrderStatusException{
		if(ModeOfBuyingEnum.ONLINE.getValue().equalsIgnoreCase(customerOrder.getModeOfBuying()) &&
				OrderStatusEnum.SHIPPED.getValue().equalsIgnoreCase(customerOrder.getOrderStatus()) && 
				OrderStatusEnum.CANCELLED.getValue().equalsIgnoreCase(orderVO.getOrderStatus())) {
			throw new InvalidOrderStatusException(ONLINE_SHIPPED_CANCELLED_ERROR_MSG);
		}
		else if(ModeOfBuyingEnum.ONLINE.getValue().equalsIgnoreCase(customerOrder.getModeOfBuying()) &&
				OrderStatusEnum.CANCELLED.getValue().equalsIgnoreCase(customerOrder.getOrderStatus()) && 
				OrderStatusEnum.SHIPPED.getValue().equalsIgnoreCase(orderVO.getOrderStatus())) {
			throw new InvalidOrderStatusException(ONLINE_CANCELLED_SHIPPED_ERROR_MSG);
		}else if(OrderStatusEnum.PRPEPARED_FOR_SHIPPING.getValue().equalsIgnoreCase(customerOrder.getOrderStatus())) {
			customerOrder.setOrderStatus(orderVO.getOrderStatus());
		}
		return customerOrder;
	}
}
