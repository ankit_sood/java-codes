package com.decathlon.orderapi.valueobjects;

public enum ModeOfBuyingEnum {
	ONLINE("Online"),STORE("Store");
	
	private String value;
	
	ModeOfBuyingEnum(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return this.value;
	}

}
