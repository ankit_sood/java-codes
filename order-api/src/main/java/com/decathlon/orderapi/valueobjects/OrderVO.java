package com.decathlon.orderapi.valueobjects;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class OrderVO {
	private Long orderId;
	private String customerName;
	private String modeOfBuying;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
	private Date orderDate;
	private String orderStatus;
}
