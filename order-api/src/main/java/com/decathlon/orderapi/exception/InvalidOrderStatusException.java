package com.decathlon.orderapi.exception;

public class InvalidOrderStatusException extends RuntimeException{
	private static final long serialVersionUID = 7734698377287339206L;

	public InvalidOrderStatusException(Long orderId,String orderStatus) {
		super("'"+orderStatus+"' not valid for order with ID '" + orderId + "'.");
	}
	
	public InvalidOrderStatusException(String message) {
		super(message);
	}
}
