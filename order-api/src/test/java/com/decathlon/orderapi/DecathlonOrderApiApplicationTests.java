package com.decathlon.orderapi;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.stream.Stream;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.h2.tools.Server;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.decathlon.orderapi.model.CustomerOrder;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT, classes = DecathlonOrderApiApplication.class)
@TestPropertySource(locations = "classpath:application-test.properties")
@SqlGroup({ @Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:schema.sql"),
		@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:data.sql")

})
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class DecathlonOrderApiApplicationTests {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate testRestTemplate;

	@Before
	public void initTest() throws SQLException {
		Server webServer = Server.createWebServer("-web", "-webAllowOthers", "-webPort", "8082");
		webServer.start();
	}

	@Test
	public void getOrdersTestById() throws JsonParseException, JsonMappingException, IOException {
		String URI = "/orders/21";
		TypeReference<CustomerOrder> t = new TypeReference<CustomerOrder>() {
		};
		ObjectMapper mapper = new ObjectMapper();
		InputStream is = DecathlonOrderApiApplicationTests.class.getResourceAsStream("/json/Order.json");
		CustomerOrder customerOrder = mapper.readValue(is, t);
		String expectedJsonResource = mapToJson(customerOrder);
		String bodyJsonResource = testRestTemplate.getForObject(formFullURLWithPort(URI), String.class);
		assertThat(bodyJsonResource).isEqualTo(expectedJsonResource);
	}

	@Test
	public void getOrdersTest() throws IOException, URISyntaxException {
		String URI = "/orders";
		Path path = Paths.get(DecathlonOrderApiApplicationTests.class.getResource("/json/Orders.json").toURI());
		StringBuilder expectedJsonResource = new StringBuilder();
		Stream<String> lines = Files.lines(path);
		lines.forEach(line -> expectedJsonResource.append(line)/*.append("\n")*/);
		lines.close();
		String bodyJsonResource = testRestTemplate.getForObject(formFullURLWithPort(URI), String.class);
		assertThat(bodyJsonResource).isEqualTo(expectedJsonResource);
	}

	@Test
	public void getOrdersTestNew() throws IOException {
		String URI = "/orders";
		InputStream is = DecathlonOrderApiApplicationTests.class.getResourceAsStream("/json/Order.json");
		ObjectMapper mapper = new ObjectMapper();

		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpGet getRequest = new HttpGet(formFullURLWithPort(URI));
		getRequest.addHeader("accept", "application/hal+json");
		HttpResponse response = httpClient.execute(getRequest);
		if (response.getStatusLine().getStatusCode() == 200) {
			response.getEntity().getContent();
		}
		String actualJsonResource = testRestTemplate.getForObject(formFullURLWithPort(URI), String.class);
	}

	/**
	 * this utility method Maps an Object into a JSON String. Uses a Jackson
	 * ObjectMapper.
	 */
	private String mapToJson(Object object) throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.writeValueAsString(object);
	}

	/**
	 * This utility method to create the url for given uri. It appends the
	 * RANDOM_PORT generated port
	 */
	private String formFullURLWithPort(String uri) {
		return "http://localhost:" + port + uri;
	}

}
