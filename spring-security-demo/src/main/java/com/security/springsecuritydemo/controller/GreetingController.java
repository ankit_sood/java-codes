package com.security.springsecuritydemo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/greet")
public class GreetingController {

	@GetMapping("/morning")
	public String morningGreetings() {
		return "Good Morning!!";
	}
	
	@GetMapping("/evening")
	public String eveningGreetings() {
		return "Good Evening!!";
	}
}
