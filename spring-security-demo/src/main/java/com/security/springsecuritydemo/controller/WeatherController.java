package com.security.springsecuritydemo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WeatherController {

	@GetMapping("/weather")
	public String getWeatherDetails() {
		return "It's a cold winter day.";
	}
}
