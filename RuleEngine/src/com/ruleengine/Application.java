package com.ruleengine;

public class Application {
	public static void main(String args[]) {
		PhoneRuleEngine phoneRuleEngine = new PhoneRuleEngine();
		phoneRuleEngine.reisterRule(new IPhone()).reisterRule(new AndroidPhone());
		Phone androidPhone = new Phone(Phone.OSType.ANDROID);
		
		Phone phone = phoneRuleEngine.rule(androidPhone);
	}
}
