package com.ruleengine;

public class IPhone implements Rule<Phone,Phone>{

	@Override
	public Phone process(Phone input) {
		input.setModel("IPhone 8");
		return input;
	}

	@Override
	public Boolean matches(Phone input) {
		return input.getOsType().equals(Phone.OSType.IOS);
	}

}
