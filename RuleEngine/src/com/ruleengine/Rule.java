package com.ruleengine;

public interface Rule<I,O> {
	Boolean matches(I input);
	O process(I input);
}
