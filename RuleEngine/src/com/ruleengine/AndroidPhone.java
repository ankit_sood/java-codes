package com.ruleengine;

public class AndroidPhone implements Rule<Phone,Phone>{
	
	@Override
	public Phone process(Phone input) {
		input.setModel("Pixel 2");
		return input;
	}

	@Override
	public Boolean matches(Phone input) {
		return input.getOsType().equals(Phone.OSType.ANDROID);
	}
}
