package com.ruleengine;

import java.util.Arrays;
import java.util.List;

public class PhoneRuleEngine {
	List<Rule<Phone,Phone>> rules;
	
	public PhoneRuleEngine() {
		rules = Arrays.asList(new IPhone(),new AndroidPhone());
	}
	
	public Phone rule(Phone phone) {
		return rules.stream().filter(rule -> rule.matches(phone))
					  .map(rule -> rule.process(phone))
					  .findFirst()
					  .orElseThrow(()->new RuntimeException("No Matching Rule found."));
	}
	
	public PhoneRuleEngine reisterRule(Rule<Phone,Phone> rule) {
		rules.add(rule);
		return this;
	}
}
