package com.datastructures.queue;
class Qnode{
	int key;
	Qnode next;
	
	public Qnode(int key){
		this.key = key;
		this.next = null;
	}
}

class Queue{
	Qnode front,rear;
	
	public Queue(){
		this.front = null;
		this.rear = null;
	}
	
	public void enqueue(int key){
		Qnode temp = new Qnode(key);
		
		if(front==null && rear==null){
			this.front = temp;
			this.rear = temp;
			return;
		}
		
		this.rear.next = temp;
		this.rear = temp;
	}
	
	public Qnode dequeue(){
		if(this.front==null){
			return null;
		}
		
		Qnode node = this.front;
		this.front = this.front.next;
		
		if(this.front==null){
			this.rear = null;
		}
		return node;
	}
}

public class QueueTest {
	public static void main(String[] args) 
    {
        Queue q=new Queue();
        q.enqueue(10);
        q.enqueue(20);
        q.dequeue();
        q.dequeue();
        q.enqueue(30);
        q.enqueue(40);
        q.enqueue(50);
         
        System.out.println("Dequeued item is "+ q.dequeue().key);
    }
}
