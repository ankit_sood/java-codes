package com.datastructures.stack;

public class Application {
	public static void main(String args[]){
		StackImpl stack = new StackImpl();
		stack.push(5);
		stack.push(10);
		stack.push(15);
		stack.push(20);
		stack.printStack();
		stack.pop();
		stack.printStack();
		stack.pop();
		stack.printStack();
		stack.pop();
		stack.printStack();
		stack.pop();
		stack.printStack();
	}
}
