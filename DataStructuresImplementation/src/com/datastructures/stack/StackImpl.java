package com.datastructures.stack;

public class StackImpl {
	private int min = Integer.MIN_VALUE;
	class Node{
		Integer key;
		Node next;
		
		public Node(Integer key){
			this.key = key;
			this.next = null;
		}
	}
	
	Node top;

	
	public void push(int key){
		Node node = new Node(key);
		if(this.top==null){
			this.top = node;
			if(key<this.min){
				this.min = key;
			}
		}else{
			node.next = this.top;
			this.top = node;
		}
	}
	
	public int pop(){
		int key = -1;
		if(this.top == null){
			System.out.println("Stack is empty!");
		}
		else{
			key = this.top.key;
			this.top = this.top.next;
		}
		return key;
	}
	
	public int getMinimum(){
		return this.getMinimum();
	}
	
	public void printStack(){
		Node temp = this.top;
		while(temp!=null){
			System.out.print(temp.key+">>>");
			temp = temp.next;
		}
		System.out.println();
	}
	
	
}
