package com.datastructures.linkedlist;

public class Application {
	public static void main(String args[]){
		LinkedList<Player> team = new LinkedList<Player>();
		Player player1 = new Player();
		player1.setId(1);
		player1.setName("Ankit Sood");
		player1.setNationality("India");
		Player player2 = new Player();
		player2.setId(2);
		player2.setName("Tony Kross");
		player2.setNationality("Germany");
		team.insertAtBegining(player1);
		team.insertAtBegining(player2);
		System.out.println("Elements in Linked List After Insertion----------->");
		team.printElements();
		System.out.println("Reversing the Elements of Linked List ----------->");
		team.reverseList();
		team.printElements();
		System.out.println("Elements in Linked list After Deletion of Indian Player ----------->");
		team.delete(player1);		
		team.printElements();
		System.out.println("Performing search for Player 2 ----------->");
		if(team.searchElement(player2)){
			System.out.println(player2.getName()+" is part of the team");
		}
	}
}
