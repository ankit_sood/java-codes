package com.datastructures.linkedlist;

public class LinkedList<T> {
	private Node<T> head;
	private Integer length = 0;
	class Node<T>{
		T data;
		Node<T> next;
		
		Node(T data){
			this.data = data;
			this.next = null;
		}
	}
	
	public void insertAtEnd(T data){
		Node<T> newData = new Node<T>(data);
		Node<T> temp = head;
		if(head==null){
			head = newData;
		}else{
			while(temp.next!=null){
				temp = temp.next;
			}
			temp.next = newData;
		}
		length++;
	}
	
	public void insertAtBegining(T data){
		Node<T> newData = new Node<T>(data);
		if(head !=null){
			newData.next = head;
		}
		head = newData;
		length++;
	}
	
	
	public boolean delete(T data){
		boolean isDeleted = false;
		Node<T> temp = head;
		if(head.data.equals(data)){
			head = head.next;
			isDeleted = true;
		}else{
			while(temp!=null && !temp.data.equals(data)){
				temp = temp.next;
			}
			if(temp!=null){
				temp = temp.next.next;
				isDeleted = true;
			}
		}
		return isDeleted;
	}
	
	public void printElements(){
		Node<T> temp = head;
		while(temp!=null){
			System.out.println(temp.data);
			temp = temp.next;
		}
	}
	
	public Integer getLength() {
		return length;
	}
	
	public boolean searchElement(T element){
		boolean isElementPresent = false;
		Node<T> temp = head;
		while(temp!=null && temp.data!= element){
			temp = temp.next;
		}
		if(temp!=null){
			isElementPresent = true;
		}
		return isElementPresent;
	}
	
	public void reverseList(){
		Node<T> temp = head;
		LinkedList<T> reversedList = new LinkedList<T>();
		while(temp!=null){
			reversedList.insertAtBegining(temp.data);
			temp= temp.next;
		}
		this.head = reversedList.head;
	}
	
}
