package com.datastructures.tree;

class Node{
	int data;
	Node left,right;
	
	public Node(int data){
		this.data = data;
		this.left = this.right = null;
	}
}

class BinarySearchTree{
	Node root;
	
	public void insert(int key){
		this.root = insertRec(this.root,key);
	}
	
	private Node insertRec(Node root,int key){
		if(root==null){
			root = new Node(key);
		}
		if(key<root.data){
			root.left = insertRec(root.left,key);
		}
		else if(key>root.data){
			root.right = insertRec(root.right,key);
		}
		return root;
	}
	
	public void inorder(){
		inorderRec(this.root);
	}
	
	private void inorderRec(Node root){
		if(root!=null){
			inorderRec(root.left);
			System.out.println(root.data);
			inorderRec(root.right);
		}
	}
	
	public Node search(int key){
		return searchRec(this.root,key);
	}
	
	public Node searchRec(Node root,int key){
		Node tempNode = null;
		if(root==null || root.data == key){
			tempNode = root;
		}else if(key>root.data){
			tempNode = searchRec(root.right,key);
		}else if(key<root.data){
			tempNode = searchRec(root.left,key);
		}
		return tempNode;
	}
}

public class BST {
	// Driver Program to test above functions
    public static void main(String[] args) {
        BinarySearchTree tree = new BinarySearchTree();
 
        /* Let us create following BST
              50
           /     \
          30      70
         /  \    /  \
       20   40  60   80 */
        tree.insert(50);
        tree.insert(30);
        tree.insert(20);
        tree.insert(40);
        tree.insert(70);
        tree.insert(60);
        tree.insert(80);
 
        // print inorder traversal of the BST
        tree.inorder();
        
        //
        Node node = tree.search(10);
        if(node!=null){
        	System.out.println("****Found "+node.data);
        }
        else{
        	System.out.println("*****Not found!");
        }
    }
}
