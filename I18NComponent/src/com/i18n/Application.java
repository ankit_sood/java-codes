package com.i18n;


import java.io.File;
import java.util.List;


public class Application {
	private static File root = new File("G:\\Test");
	private static String fileName = "package.properties";
	private static String locale = "ja";
	
	public static void main(String args[]){
		try {
			FilePath filePath = new FilePath();
			List<String> absoluteFilePathList = filePath.searchDirectory(root, fileName);
			if(absoluteFilePathList.size()!=0){
				for(String path : absoluteFilePathList){
					System.out.println("Absolute File Path: "+path);
					LanguageConvertor languageConvertor = new LanguageConvertor(path, fileName,locale);
					languageConvertor.generateConvertedPropertyFile();
					break;
				}
			}
        } catch (Exception e) {
            e.printStackTrace();
        }
	}
}
