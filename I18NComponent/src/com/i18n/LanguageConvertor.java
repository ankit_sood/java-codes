package com.i18n;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.net.ssl.HttpsURLConnection;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

public class LanguageConvertor {
	private final String absoluteFileName;
	private final String fileNamePrefix = "package";
	private final String fileNameSuffix = ".proeprties";
	private final String outputFilePath;
	private final String locale;
	
	static String BASE_URL = "https://translate.googleapis.com/translate_a/single?client=gtx&sl=en&dt=t";
	
	public LanguageConvertor(String absoluteFileName,String fileName,String locale){
		this.absoluteFileName = absoluteFileName;
		this.outputFilePath = absoluteFileName.substring(0,absoluteFileName.indexOf(fileName));
		System.out.println("******************");
		System.out.println(outputFilePath + fileNamePrefix +"_"+ "ja" + fileNameSuffix);
		this.locale = locale;
	}

	public void generateConvertedPropertyFile() throws IOException {
		InputStream inputStream = new FileInputStream(absoluteFileName);
		BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
		List<String> keys = new ArrayList<>();
		List<String> values = new ArrayList<>();
		String line = br.readLine();
		StringBuilder originalValuesCSV = new StringBuilder();
		while (line != null) {
			String[] arr = line.split("=");
			if(arr.length!=0){
				keys.add(arr[0]);
				values.add(arr[1]);
				originalValuesCSV.append(arr[1] + "|");
				System.out.println(arr[0] + "=" + arr[1]);
			}
			line = br.readLine();
		}
		br.close();
		String convertedValuesCSV = getConvertedValuesInCSV(originalValuesCSV.substring(0, originalValuesCSV.length() - 1),locale);
		if(convertedValuesCSV!=null && convertedValuesCSV.length()!=0){
			String[] convertedValues = convertedValuesCSV.split(Pattern.quote("|"));
			for (int i = 0; i < keys.size(); i++) {
				convertedValues[i] = unicodeEscaped(convertedValues[i]);
				String output = keys.get(i) + "=" + convertedValues[i] + System.getProperty("line.separator");
				System.out.print(output);
				createOutputFile(output, locale);
			}
		}
	}

	private String getConvertedValuesInCSV(String originalValuesCSV,String reqdLocale) throws IOException {
		String convertedCSV = null;
		StringBuilder URL = new StringBuilder();
		URL.append(BASE_URL).append("&tl=").append(reqdLocale);
		URL.append("&q=" + URLEncoder.encode(originalValuesCSV, "UTF-8"));
		URL url = new URL(URL.toString());
		HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
		con.setRequestProperty("User-Agent",
				"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-GB;     rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13 (.NET CLR 3.5.30729)");
		con.setRequestProperty("accept-charset", "UTF-8");
		con.setRequestProperty("content-type","application/x-www-form-urlencoded; charset=utf-8");
		con.setDoOutput(true);
		con.setRequestMethod("GET");
		System.out.println(con.getURL());
		con.connect();
		int responseCode = con.getResponseCode();
		System.out.println("GET Response Code :: " + responseCode);
		StringBuffer response = null;
		if (responseCode == HttpsURLConnection.HTTP_OK) { // success
			BufferedReader in = new BufferedReader(new InputStreamReader(
					con.getInputStream(), "UTF-8"));
			response = new StringBuffer();
			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			JsonParser jsonParser = new JsonParser();
			JsonArray arrayFromString = jsonParser.parse(response.toString())
					.getAsJsonArray();
			convertedCSV = arrayFromString.get(0).getAsJsonArray().get(0)
					.getAsJsonArray().get(0).getAsString();
			System.out.println(convertedCSV);
		}
		return convertedCSV;
	}

	private void createOutputFile(String data, String reqdLocale)
			throws IOException {
		File file = new File(outputFilePath + fileNamePrefix + "_"+ reqdLocale + fileNameSuffix);
		FileWriter fr = null;
		BufferedWriter br = null;
		try {
			fr = new FileWriter(file, true);
			br = new BufferedWriter(fr);
			br.write(data);
		} finally {
			br.close();
			fr.close();
		}
	}
	
	public static String unicodeEscaped(String nonUnicodeString) {
		StringBuffer output = new StringBuffer();
		for (char ch : nonUnicodeString.toCharArray()) {
			if (ch < 0x10) {
				output.append("\\u000" + Integer.toHexString(ch));
			} else if (ch < 0x100) {
				output.append("\\u00" + Integer.toHexString(ch));
			} else if (ch < 0x1000) {
				output.append("\\u0" + Integer.toHexString(ch));
			} else {
				output.append("\\u" + Integer.toHexString(ch));
			}

		}
		return output.toString();
	}
}
