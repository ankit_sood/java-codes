package com.i18n;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

public class FilePath {
	private String fileNameToSearch = null;
	private List<String> absoluteFilePathList = null;

	public FilePath() {
		this.absoluteFilePathList = new LinkedList<>();
	}

	private void search(File file) {
		if (file.canRead()) {
			for (File tempFile : file.listFiles()) {
				if (tempFile.isDirectory()) {
					search(tempFile);
				} else {
					if (getFileNameToSearch().equals(tempFile.getName())) {
						absoluteFilePathList.add(tempFile.getAbsolutePath());
					}
				}
			}
		}
	}

	public List<String> searchDirectory(File baseDirectory,
			String fileNameToSearch) {
		setFileNameToSearch(fileNameToSearch);
		if (baseDirectory.isDirectory()) {
			search(baseDirectory);
		}
		return absoluteFilePathList;
	}

	private String getFileNameToSearch() {
		return fileNameToSearch;
	}

	private void setFileNameToSearch(String fileNameToSearch) {
		this.fileNameToSearch = fileNameToSearch;
	}

}